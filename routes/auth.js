const jwt = require("jwt-simple");

const auth = async (req, res, next) => {
  try {
    const token = req
      .header("Authorization")
      .replace("Bearer", "")
      .trim();
    var decoded = jwt.decode(token, "shehzad");
    console.log(decoded); //=> { foo: 'bar' }

    next();
  } catch (error) {
    console.log(error);
    res.status(401).send({ message: "Please authenticate!" });
  }
};

module.exports = auth;
