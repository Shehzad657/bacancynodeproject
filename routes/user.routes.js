const fs = require('fs');
const express = require("express");
const auth = require('./auth');

const router = express.Router();

const userController = require('../controllers/users.controller')

router.post('/add', userController.addUser);

router.post('/login', userController.loginUser);

router.get('/getUserList',auth, userController.getUserList);


module.exports = router;
