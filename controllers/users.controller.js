const fs = require("fs");
const jwt = require("jwt-simple");

exports.addUser = (req, res) => {
  let data = JSON.parse(req.body.data);

  let message = "";
  let  { firstName, lastName, password, username, mobileNumber, address, dateOfBirth } = data;
  
  let uploadedFile = req.files.file;
  let image_name = uploadedFile.name;
  let fileExtension = uploadedFile.mimetype.split("/")[1];
  image_name = username + "." + fileExtension;

  let usernameQuery = "SELECT * FROM `users` WHERE user_name = '" + username + "'";

  db.query(usernameQuery, (err, result) => {
    if (err) {
      console.log(err);
      return res.status(500).send(err);
    }

    if (result.length > 0) {
      message = "Username already exists";
      return res.status(500).send({ message: message });
    } else {
      if (
        uploadedFile.mimetype === "image/png" ||
        uploadedFile.mimetype === "image/jpeg" ||
        uploadedFile.mimetype === "image/gif"
      ) {
        // upload the file to the /public/assets/img directory
        uploadedFile.mv(`public/assets/img/${image_name}`, err => {
          if (err) {
            return res.status(500).send(err);
          }

          let query = `INSERT INTO users (first_name, last_name, password, user_name,address,mobileNumber,dateOfBirth,image) VALUES (
              '${firstName}', '${lastName}', '${password}', '${username}', '${address}' , '${mobileNumber}' , '${dateOfBirth}','http://localhost:2000/${image_name}')`;

          console.log(query);
          db.query(query, (err, result) => {
            if (err) {
              return res.status(500).send(err);
            }
            return res.status(200).send({ message: "user added successfully" });
          });
        });
      } else {
        message = "Invalid File format. Only 'gif', 'jpeg' and 'png' images are allowed.";
        return res.status(500).send(message);
      }
    }
  });
};

exports.loginUser = (req, res) => {
  let { username, password } = req.body;

  let usernameQuery = `SELECT * FROM users WHERE user_name = '${username}' and password = '${password}'`;

  db.query(usernameQuery, (err, result) => {
    if (err) {
      console.log(err);
      return res.status(500).send(err);
    }

    if (result.length) {
      const payload = { res: result };
      const secret = "shehzad";

      const token = jwt.encode(payload, secret);
      return res.status(200).send({ res: result, token: token });
    } else {
      return res.status(500).send({ message: "Invalid Username or Password." });
    }
  });
};

exports.getUserList = (req, res) => {
  let query = `SELECT first_name, last_name,user_name,id,image FROM users`;

  db.query(query, (err, result) => {
    if (err) {
      console.log(err);
      return res.status(500).send(err);
    }

    if (result.length) {
      return res.status(200).send(result);
    } else {
      return res.status(500).send({ message: "invalid user name" });
    }
  });
};
